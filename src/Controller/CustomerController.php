<?php

namespace App\Controller;

use App\Entity\Customer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class CustomerController extends Controller
{
    /**
     * @Route("/customer", name="customer",methods={"POST"})
     * @param EntityManagerInterface $entityManager
     * @param UserPasswordEncoderInterface $encoder
     * @return Response
     */
    public function dummyUser(EntityManagerInterface $entityManager, UserPasswordEncoderInterface $encoder): Response
    {
        $customer = new Customer();
        $customer->setUsername('dummy');
        $customer->setPassword($encoder->encodePassword($customer, '123456'));
        $entityManager->persist($customer);
        $entityManager->flush();

        return $this->json(
            [
                'message' => 'username is dummy and password is 123456',
            ]
        );
//            09032456998
    }

}
