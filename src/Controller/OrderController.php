<?php

namespace App\Controller;

use App\Entity\Order;
use App\Form\OrderType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OrderController extends Controller
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }


    /**
     * @param Request $request
     * @Route("/api/order", name="order.store",methods={"POST"})
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function store(Request $request)
    {
        $order = new Order();
        $form = $this->createForm(OrderType::class, $order, ['method' => 'POST']);
        $form->submit($request->request->all());
        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($order);
            $this->entityManager->flush();

            return $this->respond($order, 201);
        }

        return $this->respondValidatorFailed($form);
    }

    /**
     * @Route("/api/order/{id}", name="order.update", methods={"PUT","PATCH"})
     * @param Order $order
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function update($id, Request $request)
    {
        /** @var Order $order */
        $order = $this->entityManager->getRepository(Order::class)->find($id);
        if ($order->getShippingDate() > new \DateTime()) {
            $form = $this->createForm(OrderType::class, $order, ['method' => $request->getMethod()]);
            $form->submit($request->request->all(), $request->isMethod('PUT'));
            if ($form->isSubmitted() && $form->isValid()) {
                $this->entityManager->persist($order);
                $this->entityManager->flush();

                return $this->respond($order, 200);
            }

            return $this->respondValidatorFailed($form);
        }

        return $this->json(
            [
                'message' => 'The shipping date is passed.',
            ]
        );
    }

    /**
     * @Route("/api/order/{id}", name="order.show",methods={"GET"})
     * @param $id
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function show($id)
    {
        $order = $this->entityManager->getRepository(Order::class)->find($id);

        return $this->respond($order);
    }

    /**
     * @Route("/api/order", name="order.list",methods={"GET"})
     * @param $id
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function index()
    {
        $order = $this->entityManager->getRepository(Order::class)->findAll();

        return $this->respond($order);
    }
}
