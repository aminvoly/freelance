<?php

namespace App\Form;

use App\Entity\Order;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('orderCode')->add('productId')->add('quantity')->add('address')->add(
            'shippingDate',
            DateTimeType::class,
            [
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class'      => Order::class,
                'csrf_protection' => false,
            ]
        );
    }
}
