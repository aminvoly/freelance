### Things to do before running project:
After cloning the project config database parameters in .env
```
composer install
./bin/console doctrine:database:create
./bin/console make:migration
./bin/console doctrine:migrations:migarte 
$ mkdir -p config/jwt
$ openssl genpkey -out config/jwt/private.pem -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096
$ openssl pkey -in config/jwt/private.pem -out config/jwt/public.pem -pubout
```
Enter the JWT_PASSPHRASE in .env
Send a POST request to /customer with empty body to create a test customer
Send a POST request to /api/login_check with username and password to get a jwt token.
### Curl example
```
curl -X POST -H "Content-Type: application/json" http://freelance.develop/api/login_check -d '{"username":"dummy","password":"123456"}'
```
### Routes
```
POST request /api/order to create a order
PUT or PATCH request /api/order/{id} to update a order
GET request /api/order/{id} to show the detail of a order
GET request /api/order to show the list of orders
```
